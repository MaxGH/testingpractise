import pytest_socket
import pytest
import socket

"""
Call code logic if you have the server setup in another function.
"""


@pytest.fixture()  # This allows functions below to use this as an argument.
def server():
    from server import run
    run(host="localhost", port=8080)


# tests that the socket is enabled through the argument.
def test_explicitly_enable_socket(socket_enabled):
    assert socket.socket(socket.AF_INET, socket.SOCK_STREAM)


# decorator makes it so you do not need to use an argument.
@pytest.mark.enable_socket
def test_explicitly_enable_socket_with_mark():
    assert socket.socket(socket.AF_INET, socket.SOCK_STREAM)



