"""
Follows the 'Arrange > Act > Assert' model.
"""
import builtins


def user_name():
    """
    Mock to test whether the username will only return letters, no numbers.
    Returns
    -------
    the users name they input
    """
    name = input("What is your name?")
    if not name.isalpha():
        print("Please only use the letters A-Z")
    else:
        pass

    return name


def test_name_input_correct(monkeypatch):
    """
    monkeypatch fixture helps you to safely set/delete an attribute, dictionary item or environment variable
    Parameters
    ----------
    monkeypatch

    Returns
    -------

    """
    # monkeypatch the "input" function, so that it returns "Max".
    # This simulates the user entering your inputs into the terminal:"
    monkeypatch.setattr("builtins.input", lambda _: "Max")
    # make a call to the username function
    name = user_name()

    if name.isalpha():
        assert True
    else:
        assert False

    assert name == "Max"


def test_holiday_energy_options(monkeypatch):
    pass